node default {
        include base
        include apt 
        include apt::update 
}

node 'devwebgw01' inherits default {
        include haproxy
}

node 'devweb01' inherits default {
	include apache2
}

node 'devweb02' inherits default {
	include apache2
}

node 'devweb03' inherits default {
	include apache2
}
