# Ensures haproxy exists, is enabled and configured to serve the webfarm.
class haproxy {
        package { 'haproxy':
                name    => 'haproxy',
                ensure  => present
        }

	service { 'haproxy':
		ensure => running,
	}

	file { '/etc/haproxy/haproxy.cfg':
                path            => '/etc/haproxy/haproxy.cfg',
                source          => 'puppet:///modules/haproxy/haproxy.cfg',
                ensure          => file,
                mode            => 0644,
                owner           => root,
                group           => root,
                require         => Package['haproxy'],
        }

		file { '/etc/default/haproxy':
                path            => '/etc/default/haproxy',
                source          => 'puppet:///modules/haproxy/haproxy.default',
                ensure          => file,
                mode            => 0644,
                owner           => root,
                group           => root,
                require         => Package['haproxy'],
        }

	
}

