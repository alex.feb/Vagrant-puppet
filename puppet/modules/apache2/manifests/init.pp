# Adds apache to the web servers.
# Also uses puppet templating to create custom webpages for each webserver.
class apache2 {
         package { 'apache2':
                name    => 'apache2',
                ensure  => present
        }
	 file    { '/var/www/index.html':
                path            => '/var/www/index.html',
                content         => template('apache2/index.html.erb'),
                ensure          => file,
                mode            => 0644,
                owner           => www-data,
                group           => www-data,
                require         => Package['apache2'],
        }

        
 }
