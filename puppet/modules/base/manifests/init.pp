# Changes the message of the day to a uniform one.
class base {
	File { owner => 0, group => 0, mode => 0644 }
 
	file { '/etc/motd':
     		content => "Welcome to your Vagrant-built virtual machine..managed by Puppet.\n"
	}

}
