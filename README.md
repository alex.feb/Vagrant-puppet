This is version 1.0 of my "Web Farm in a box" project:

I admit it. This project started as a fun idea to create a web server in puppet. 
Then I thought about provisioning... then I started getting fascinated by vagrant. 
I stopped before configuring nagios as well as observium.
I might revisit, but next time using something like kubernetes. 

Requirements : 
Virtual box, Vagrant 1.7.4

Once Built: 
Go to http://127.0.0.1:4567 to access. 